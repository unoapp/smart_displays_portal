/* eslint-disable space-before-function-paren */
/* eslint-disable indent */
import jsPDF from 'jspdf'
import html2canvas from 'html2canvas'
export default {
    // toPDF (canvas, filename) {
    //   let doc = new jsPDF('l', 'mm', 'a4')
    //   let width = doc.internal.pageSize.getWidth()
    //   let height = doc.internal.pageSize.getHeight()
    //   doc.addImage(canvas, 'PNG', 0, 0, width, height)
    //   doc.save(`${filename}.pdf`)
    // },
    async toPDFWithDiv(div, filename) {
        let canvas
        try {
            canvas = await html2canvas(div, {
                useCORS: true,
                optimized: false,
                allowTaint: false
            })
        } catch (error) {
            console.log(error)
            throw error
        }
        var doc = new jsPDF('p', 'mm', 'pdf', 0)
        var imgData = canvas.toDataURL('image/png')
        var imgWidth = doc.internal.pageSize.getWidth()
        var pageHeight = doc.internal.pageSize.getHeight()
        var imgHeight = canvas.height * imgWidth / canvas.width
        var heightLeft = imgHeight
        var position = 0
        console.log(canvas.height)
        doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight)
        heightLeft -= pageHeight
        while (heightLeft >= 0) {
            position = heightLeft - imgHeight
            doc.addPage()
            doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight)
            heightLeft -= pageHeight
        }
        doc.save(`${filename}.pdf`)
    }
}
