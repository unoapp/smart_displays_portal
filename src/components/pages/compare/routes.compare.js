import Dashboard from './compare.vue'
import { auth } from '@/router/middlewares/auth'

// import middlewares
export default [{
  path: '/compare',
  component: Dashboard,
  name: 'Compare',
  meta: {
    middleware: [auth],
    nav: {
      display: true,
      name: 'Compare',
      icon: 'dashboard'
    }
  }
}]
