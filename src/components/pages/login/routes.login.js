import Login from './login.vue'
import { redirectToDashboard } from '@/router/middlewares/redirecttodashboard'

// import middlewares
export default [{
  path: '/login',
  alias: '/',
  component: Login,
  name: 'Login',
  meta: {
    middleware: [redirectToDashboard],
    nav: {
      display: false,
      name: 'Login'
    }
  }
}]
