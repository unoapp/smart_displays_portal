import Dashboard from './dashboard'
import { auth } from '@/router/middlewares/auth'

// import middlewares
export default [{
  path: '/dashboard',
  component: Dashboard,
  name: 'Overview',
  meta: {
    middleware: [auth],
    nav: {
      display: true,
      name: 'Overview',
      icon: 'fas fa-columns'
    }
  }
}]
