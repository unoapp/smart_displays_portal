import axios from 'axios'
import { URL, apiToken, authToken } from './auth.env'

const authAxiosInstaceV1 = axios.create({
  baseURL: `${URL}/api/v1`,
  headers: {
    'api-token': apiToken
  }
})

export const AuthAPI = {

  /**
   * Login user with Email and Password
   * @param {string} [data={ email: '', password: '' }]
   * @returns {Promise}
   */
  login(data = { email: '', password: '' }) {
    return authAxiosInstaceV1({
      method: 'post',
      data: data,
      url: `/users/login`,
      headers: {
        'api-token': apiToken
      }
    })
  },
  /**
   *
   * Login user with userToken
   * @param {*} token
   * @returns
   */
  loginWithToken(token) {
    return authAxiosInstaceV1({
      method: 'get',
      url: `/users/profile`,
      headers: {
        'auth-token': token,
        'api-token': apiToken
      }
    })
  },
  /**
   * Call to get list of user businesses
   *
   * @returns {Promise}
   */
  getUserBusinesses() {
    return authAxiosInstaceV1({
      method: 'get',
      url: '/users/businesses',
      headers: {
        'auth-token': authToken(),
        'api-token': apiToken
      }
    })
  },
  /**
   * Call to set users active business
   *
   * @param {*} businessId
   * @returns {Promise}
   */
  setActiveBusiness(businessId) {
    return authAxiosInstaceV1({
      method: 'patch',
      url: `/users/businesses/${businessId}`,
      headers: {
        'auth-token': authToken(),
        'api-token': apiToken
      }
    })
  },
  /**
   *  Call to get a business by id
   *
   * @returns {Promise}
   */
  getBusinessById(businessId) {
    return authAxiosInstaceV1({
      method: 'get',
      url: `/businesses/${businessId}`,
      headers: {
        'auth-token': authToken(),
        'api-token': apiToken
      }
    })
  },
  logout() {
    return authAxiosInstaceV1({
      method: 'post',
      url: `/users/logout`,
      headers: {
        'auth-token': authToken(),
        'api-token': apiToken
      }
    })
  }

}
