import axios from 'axios'
import { URL, apiToken, authToken } from './locations.env'

const locationsAxiosInstaceV1 = axios.create({
  baseURL: `${URL}`,
  headers: {
    'api-token': apiToken
  }
})

export const LocationsAPI = {
  getLocations(value) {
    return locationsAxiosInstaceV1({
      method: 'POST',
      data: value,
      url: `/locations/grouped`,
      headers: {
        'api-token': apiToken,
        'auth-token': authToken()
      }
    })
  }
}
