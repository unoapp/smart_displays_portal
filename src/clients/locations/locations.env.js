export const URL = process.env.VUE_APP_LOCATIONS_URL
export const apiToken = process.env.VUE_APP_LOCATIONS_API_TOKEN
export const authToken = () => JSON.parse(localStorage.getItem('vuex')).auth.user.token
