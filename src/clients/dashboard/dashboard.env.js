export const URL = process.env.VUE_APP_SMART_URL
export const apiToken = process.env.VUE_APP_SMART_TOKEN
export const authToken = () => JSON.parse(localStorage.getItem('vuex')).auth.user.token

export const SMART_URL = process.env.VUE_APP_SMART_RACKS_URL
export const smartApiToken = process.env.VUE_APP_SMART_RACKS_API_TOKEN
