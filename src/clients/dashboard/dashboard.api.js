import axios from 'axios'
import { URL, apiToken, authToken, SMART_URL, smartApiToken } from './dashboard.env'

const dashboardAxiosInstaceV1 = axios.create({
  baseURL: `${URL}/api/v1`,
  headers: {
    'api-token': apiToken
  }
})

export const DashboardAPI = {

  getDashboardSummary(value) {
    return dashboardAxiosInstaceV1({
      method: 'POST',
      data: value.data ? value.data : { locations_ids: '' },
      url: `/summaries/dashboard`,
      headers: {
        'api-token': apiToken,
        'auth-token': authToken()
      },
      params: {
        start_date: value.startDate ? value.startDate : '2019-11-01',
        end_date: value.endDate ? value.endDate : '2019-12-31'
      }
    })
  },

  getLocationGroup(value) {
    return dashboardAxiosInstaceV1({
      method: 'GET',
      url: `/users/locations`,
      headers: {
        'api-token': apiToken,
        'auth-token': authToken()
      },
      params: {
        locations_only: value.locations_only ? value.locations_only : false,
        groups: value.groups ? value.groups : 1
      }
    })
  },

  getBaseDataSummary(value) {
    return dashboardAxiosInstaceV1({
      method: 'POST',
      data: value.data ? value.data : { locations_ids: '' },
      url: `/summaries/comparison/base`,
      headers: {
        'api-token': apiToken,
        'auth-token': authToken()
      },
      params: {
        start_date: value.startDate ? value.startDate : '2019-11-01',
        end_date: value.endDate ? value.endDate : '2019-12-31'
      }
    })
  },

  getComparisionDataSummary(value) {
    return dashboardAxiosInstaceV1({
      method: 'POST',
      data: value.data ? value.data : { locations_ids: '' },
      url: `/summaries/comparison/data`,
      headers: {
        'api-token': apiToken,
        'auth-token': authToken()
      },
      params: {
        start_date: value.startDate ? value.startDate : '2019-11-01',
        end_date: value.endDate ? value.endDate : '2019-12-31'
      }
    })
  },

  getSalesForManyLocations(value) {
    let url = `${SMART_URL}/api/v2/pos/businesses/${value.data.business_uuid}/locations/report`
    let data = value.data ? value.data : { locations_ids: '' }
    let config = {
      params: {
        start_date: value.startDate ? value.startDate : '2018-10-01',
        end_date: value.endDate ? value.endDate : '2019-12-31'
      },
      headers: {
        'api-token': smartApiToken,
        'auth-token': authToken()
      }

    }
    return axios.post(url, data, config)
  }
}
