import axios from 'axios'
import { URL, apiToken, authToken } from './wifi_tracking.env'

const wifiAxiosInstaceV1 = axios.create({
  baseURL: `${URL}/api/v1`,
  headers: {
    'api-token': apiToken
  }
})

export const WifiAPI = {
  getDashboardSummary(value) {
    return wifiAxiosInstaceV1({
      method: 'POST',
      data: value.data,
      url: `summaries/dashboard`,
      headers: {
        'api-token': apiToken,
        'auth-token': authToken()
      },
      params: {
        start_date: value.startDate,
        end_date: value.endDate
      }
    })
  }
}
