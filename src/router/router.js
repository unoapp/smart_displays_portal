import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store/store'
import middlewarePipeline from './middlewarepipeline'

// routes
import Login from '@/components/pages/login/routes.login'
import Overview from '@/components/pages/dashboard/routes.dashboard'
import Compare from '@/components/pages/compare/routes.compare'

/* Global Components */
/* -----Offset Heading----- */
import OffsetHeading from '@/components/custom-components/OffsetHeading'
Vue.component('offset-heading', OffsetHeading)
Vue.component('offset-block', () => import('@/components/custom-components/OffsetBlock'))
Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [...Login, ...Overview, ...Compare]
})

router.beforeEach((to, from, next) => {
  if (!to.meta.middleware || to.meta.middleware.length <= 0) {
    return next()
  }
  const middleware = to.meta.middleware
  const context = {
    to,
    from,
    next,
    store
  }

  return middleware[0]({
    ...context,
    next: middlewarePipeline(context, middleware, 1)
  })
})

export default router
