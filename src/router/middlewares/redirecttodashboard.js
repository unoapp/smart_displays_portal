
export const redirectToDashboard = function ({ to, next, store }) {
  const user = store.getters['auth/user']
  let notLoggedIn = !user || user === undefined || user === null
  if (!notLoggedIn) {
    return next({
      name: 'Overview'
    })
  } else {
    return next()
  }
}
