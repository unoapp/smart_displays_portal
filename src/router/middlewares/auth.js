// auth middle ware to check if the user is logged in
export const auth = function ({ to, next, store }) {
  const user = store.getters['auth/user']
  let notLoggedIn = !user || user === undefined || user === null || user === ''
  if (notLoggedIn) {
    return next({
      name: 'Login'
    })
  } else {
    return next()
  }
}
