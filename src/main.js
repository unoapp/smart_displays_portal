/* eslint-disable no-unused-vars */
import Vue from 'vue'
import App from './App.vue'
import router from './router/router'
import store from './store/store'
import vuetify from './vuetify/plugin.vuetify'
import * as VueGoogleMaps from 'vue2-google-maps'
import '../node_modules/material-design-icons-iconfont/dist/material-design-icons.css'
import '@fortawesome/fontawesome-free/css/all.css'

// mixins
import { mixin } from './mixins/mixins.js'

// components

import { googleMapApi } from '@/store/modules/dashboard/env.js'

Vue.mixin(mixin)

Vue.config.productionTip = false

Vue.use(VueGoogleMaps, {
  load: {
    key: googleMapApi
  }
})

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App),
  data() {
    return {
      spinners: {
        main: false
      },
      displayAll: false

    }
  }
}).$mount('#app')
