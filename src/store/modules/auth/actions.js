import { AuthAPI } from '@/clients/authclient/auth.api'

import {
  SET_USER,
  SET_BUSINESSES_LIST,
  SET_ACTIVE_BUSINESS,
  SET_ACTIVE_BUSINESS_BRANDING,
  CLEAR_AUTH_STATE
} from '@/store/mutation-types'

export default {
  async loginUser({ commit }, loginData) {
    const { data: { payload } } = await AuthAPI.login(loginData)
    const { user, token } = payload
    user.token = token
    commit(SET_USER, user)
    return payload
  },
  async loginWithToken({ commit }, token) {
    const { data: { payload } } = await AuthAPI.loginWithToken(token)
    const { user } = payload
    user.token = token
    commit(SET_USER, user)
    return payload
  },
  async setUserBusinessesList({ commit }) {
    const { data: { payload } } = await AuthAPI.getUserBusinesses()
    commit(SET_BUSINESSES_LIST, payload)
    return payload
  },
  async setActiveBusiness({ commit }, businessId) {
    const { data: { payload } } = await AuthAPI.setActiveBusiness(businessId)
    commit(SET_ACTIVE_BUSINESS, payload)
    return payload
  },
  async setActiveBusinessBranding({ commit }, businessId) {
    const { data: { payload } } = await AuthAPI.getBusinessById(businessId)
    commit(SET_ACTIVE_BUSINESS_BRANDING, payload)
  },
  async clearState({ commit }) {
    console.log('clear setate')
    commit(CLEAR_AUTH_STATE)
  },
  async logOut() {
    console.log('logging our')
    await AuthAPI.logout()
  }
}
