export default {
  user: (state) => state.user,
  locationsList: (state) => state.locationsList,
  selectedLocations: (state) => state.selectedLocations,
  businessesList: state => state.businessesList,
  activeBusiness: state => state.activeBusiness,
  activeBusinessBrandingAndLogo: (state) => {
    if (state.activeBusinessBranding.branding) {
      return {
        primary: state.activeBusinessBranding.branding.brand_colors.primary,
        secondary: state.activeBusinessBranding.branding.brand_colors.secondary,
        accent: state.activeBusinessBranding.branding.brand_colors.accent,
        logo: state.activeBusinessBranding.branding.logo === '' ? require('@/assets/images/uno-logo.png') : state.activeBusinessBranding.branding.logo
      }
    }
    return {
      primary: '#F03C0C',
      secondary: '#333333',
      accent: '#ea9780',
      logo: require('@/assets/images/uno-logo.png')
    }
  }
}
