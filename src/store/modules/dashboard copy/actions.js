import * as types from './node_modules/@/store/mutation-types'
import { DashboardAPI } from './node_modules/@/clients/dashboard/dashboard.api'

export default {
    async GetDashboardSummary({ commit }, value) {
        const { data: { payload } } = await DashboardAPI.getDashboardSummary(value)
        commit(types.SET_DASHBOARD_SUMMARY, payload)
        return payload
    },

    async GetLocationGroup({ commit }, value) {
        const { data: { payload } } = await DashboardAPI.getLocationGroup(value)
        commit(types.SET_LOCATION_GROUP, payload)
        return payload
    }
}
