import * as types from '@/store/mutation-types'
import { WifiAPI } from '@/clients/wifi tracking/wifi_tracking.api'

export default {
    async GetDashboardSummary({ commit }, value) {
        const { data: { payload } } = await WifiAPI.getLocations(value)
        commit(types.SET_WIFI_TRACKING_DASHBOARD, payload)
        return payload
    }
}
