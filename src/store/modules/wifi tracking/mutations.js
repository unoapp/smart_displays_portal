import * as types from '@/store/mutation-types'

export default {
    [types.SET_WIFI_TRACKING_DASHBOARD](state, data) {
        state.dashboard = data
    }
}
