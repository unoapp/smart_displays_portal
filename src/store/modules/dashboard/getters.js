export default {
    asset_locations: state => state.dashboard.asset_locator,
    asset_status: state => state.dashboard.assets,
    assets: state => state.dashboard.assets,

    weekly: state => state.dashboard.weekly,
    daily: state => state.dashboard.daily,
    monthly: state => state.dashboard.monthly,
    national: state => state.dashboard.national_data,

    dashboard: state => state.dashboard,

    baseData: state => state.baseData,
    comparisionData: state => state.comparisionData,
    salesForManyLocations: state => state.salesForManyLocations

}
