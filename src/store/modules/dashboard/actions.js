import * as types from '@/store/mutation-types'
import { DashboardAPI } from '@/clients/dashboard/dashboard.api'

export default {
    async GetDashboardSummary({ commit }, value) {
        console.log(value)
        try {
            const { data: { payload } } = await DashboardAPI.getDashboardSummary(value)
            commit(types.SET_DASHBOARD_SUMMARY, payload)
            return payload
        } catch (error) {
            console.log(error)
        }
    },

    async GetLocationGroup({ commit }, value) {
        const { data: { payload } } = await DashboardAPI.getLocationGroup(value)
        commit(types.SET_LOCATION_GROUP, payload)
        return payload
    },

    async GetBaseDataSummary({ commit }, value) {
        const { data: { payload } } = await DashboardAPI.getBaseDataSummary(value)
        commit(types.SET_BASE_DATA_SUMMARY, payload)
        return payload
    },

    async GetComparisionDataSummary({ commit }, value) {
        const { data: { payload } } = await DashboardAPI.getComparisionDataSummary(value)
        commit(types.SET_COMPARION_DATA_SUMMARY, payload)
        return payload
    },

    async GetSalesForManyLocations({ commit }, value) {
        const { data: { payload } } = await DashboardAPI.getSalesForManyLocations(value)
        commit(types.SET_SALES_FOR_MANY_LOCATIONS, payload)
        console.log(payload, 'payload')
        return payload
    }

}
