import * as types from '@/store/mutation-types'

export default {
    [types.SET_DASHBOARD_SUMMARY](state, data) {
        state.dashboard = data
    },

    [types.SET_BASE_DATA_SUMMARY](state, data) {
        state.baseData = data
    },

    [types.SET_COMPARION_DATA_SUMMARY](state, data) {
        state.comparisionData = data
    },

    [types.SET_LOCATION_GROUP](state, data) {
        state.locationGroup = data
    },

    [types.SET_SALES_FOR_MANY_LOCATIONS](state, data) {
        state.salesForManyLocations = data
    }

}
