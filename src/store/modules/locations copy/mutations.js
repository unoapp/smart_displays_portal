import * as types from './node_modules/@/store/mutation-types'

export default {
    [types.SET_GROUPED_LOCATIONS](state, data) {
        state.locations = data
    }
}
