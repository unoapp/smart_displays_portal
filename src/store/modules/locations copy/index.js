import actions from './actions.js.js'
import getters from './getters.js.js'
import state from './state.js.js'
import mutations from './mutations.js.js'

export default {
    namespaced: true,
    actions,
    getters,
    state,
    mutations
}
