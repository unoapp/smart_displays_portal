import * as types from '@/store/mutation-types'
import { LocationsAPI } from '@/clients/locations/locations.api'

export default {
    async GetLocations({ commit }, value) {
        console.log(value)
        const { data: { payload } } = await LocationsAPI.getLocations(value)
        commit(types.SET_GROUPED_LOCATIONS, payload)
        return payload
    }
}
