import * as types from '@/store/mutation-types'

export default {
    [types.SET_GROUPED_LOCATIONS](state, data) {
        state.locations = data
    }
}
