import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

// state
import Snackbar from '@/components/custom-components/snackbar/snackbar.state'
import auth from './modules/auth/index'
import dashboard from './modules/dashboard/index'
import locations from './modules/locations/index'
import wifiTracking from './modules/wifi tracking/index'

const initialState = {
  Snackbar: Snackbar.state,
  auth: auth.state,
  dashboard: dashboard.state,
  locations: locations.state,
  wifiTracking: wifiTracking.state
}
Vue.use(Vuex)
console.log('initialState', initialState.dashboard)
const store = new Vuex.Store({
  plugins: [createPersistedState()],
  modules: { Snackbar, auth, dashboard, locations, wifiTracking },
  mutations: {
    CLEAR_STATE(state) {
      Object.keys(state).forEach((key) => {
        Object.assign(state[key], initialState[key])
      })
      auth.state.user = {}
      dashboard.state.dashboard = ''
      console.log('clearing store')
    }
  }
})

export default store
