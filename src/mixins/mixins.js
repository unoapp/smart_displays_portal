import store from '@/store/store'

export const mixin = {
  methods: {
    snackbar ({ text = '', open = true, timeout = 3000 }) {
      store.dispatch('Snackbar/snackbar', { text, open, timeout })
    },
    spinner (state) {
      this.$root.spinners.main = state
    },
    createChartDialog (state) {
      this.$root.dialogs.createChart = state
    }
  }
}
